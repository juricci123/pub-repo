MT4 EA Requirement
----------------------------
```
Name: Stochastic-Divergence.mq4
```

Requirements:
--------------------
```
Program EA preferably eather than Indicator.
Ea should trigger a divergence based on:
The python program should integrate with Google or Yahoo API to retrieve Stock price and perform analysis of the Stock
Example Backtest Strategy: http://www.pythonforfinance.net/2017/10/10/stochastic-oscillator-trading-strategy-backtest-in-python/#more-15863
Stochastic Indicator Details: https://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:stochastic_oscillator_fast_slow_and_full
```

1-Where to run
--------------------

```Able to run from Linux server: Ubuntu or CentOS```

2-Script should have default variables:
--------------------
```
SK=14 (for Stochastic K)
SD=3 (for Stochastic D)
SKLValue=60 (for Stochastic K when looking for Long positions)
PT=2 (for ProfitTarget, we want to risk $1 and make $2) 
```

3-Scan feature should trigger as follows:
-----------------------------------------

```
ONLY Long positions:
If (Slow Stochastic K crosses above Slow Stochastic D); then
Store Current Stock Close Price and Current Stochastic K Value
Both these values needs to be compared with the previous CrossOver for the following critirias:
If (currentCrossKValue > previousCrossKValue) &&
If (currentStockClosePrice < previousStockClosePrice) &&
currentStochasticsKValue < SKLValue
Send alert text
```

4-Backtest feature
------------------

```
Same as above but instead of Sending Alert, we would enter
A long position. We would set StopLoss to Low Price of previous bar
And set target to 2x StopLoss which is: (Current Close Price - LowPrice of previous bar) x 2) + (current Stock Price)
```
